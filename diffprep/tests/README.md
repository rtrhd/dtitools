Diffprep Tests
==============

Unit Tests
----------

 - `test_diffprep_unit_0`: fast unit tests not erltying on data directory
 - `test_diffprep_unit_1A`: fast tests on first data set
 - `test_diffprep_unit_1B`: fast tests on second data set
 - `test_diffprep_unit_1C`: fast tests on first data set (zoomit acquisition)
 - `test_diffprep_unit_2`: further fast tests on second data set
 - `test_diffprep_unit_3`: slow tests that actually run topup and eddy on all three datasets

Script Level Tests
------------------
 - `test_diffprep_script`: slow tests that run diffprep as script on all three datasets
   - leaves result in output directories for further analysis

Manual Tests
-------------------

 - `notes_on_diffprep_test_results.ipynb`: Visual presentation of results
 - Submission to grid engine
 - Restriction in number of openmp threads
 - Correct convention for gradient polarities on both y and x axes

R.Hartley-Davies June 2018

