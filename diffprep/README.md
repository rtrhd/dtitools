Diffprep
========

Shell Script for Preparing Up/Down DTI scans using TopUp and Eddy
-----------------------------------------------------------------

Use the `Makefile` to install `diffprep` to `/usr/local/bin`. It depends on an [FSL](http://fsl.fmrib.ox.ac.uk/) installation. The environment variable `$FSLDIR` should refer to the `FSL` installation directory.

 
Usage:
:    diffprep -u `<upfile>` -d `<downfile>` [-p `<phaseenc>`] [-t `<readouttime>`] [-n `<numthreads>`] [-s] [-v] [-h]

Options:
:    -u name of blip up volume nifti file (the .nii.gz is optional)
:    -d name of blip down volume nifti file (the .nii.gz is optional)
:    -t epi read out time in seconds (taken from nifti headers if not specified)
:    -p phase encoding direction in images (ROW or COL (ie X or Y) - default is COL)
:    -n maximum number of openmp threads to allow for the fsl eddy command (default is 2)
:    -s standalone, run directly rather than submitting via fsl_sub
:    -v verbose, print extra logging information
:    -h print this message

Environment:
:    FSLDIR must refer to a working FSL distribution.
:    Setting OMP_NUM_CORES will determine the number of openmp threads irrespective of the command line argument.
:    If DP_STANDALONE is not set (or the '-s' flag given) the script will submit itself to the batch system using fsl_sub 
<br>

The script asssumes that all nifti image files are in 4D compressed form ('.nii.gz'). The default EPI readout
time is based on the dwell time that [dcm2nii](http://www.mccauslandcenter.sc.edu/mricro/mricron/dcm2nii.html) puts in the `pixdim[6]` field of the [nifti](http://nifti.nimh.nih.gov/nifti-1) header. The intention here is that it should be the reciprocal of the phase encode pixel bandwidth in the DICOM header. The DICOM phase encode *axis* is not represented in the nifti header so must be specified or assumed.

The script uses the two fsl commands [topup](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/TOPUP) and [eddy](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/EDDY) to correct for distortion in the EPI direction due to off-resonance and eddy currents respectively using a pair of acquisitions with opposite EPI readout polarities.

There is a version of the eddy command written to utilise shared memory parallelism using the [openmp](http://en.wikipedia.org/wiki/OpenMP) library.

The number of parallel threads can be controlled by the '-n' option. The default is less than the typical number of cores available to prevent eddy from swamping individual workstations.

We do not specify the openmp of eddy explicitly here but rely on the version installed on the system path so expect this version of eddy to have been installed over the standard single threaded one supplied in neurodebian.

Although neither `topup` nor `eddy` make use of the job parallelism for which the fsl batch system is intended, this script can take many hours to run and so will normally still be used with a batch system.
In view of this the script submits itself by default using [fsl_sub](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SGE%20submission%20FAQ) in the same way as a number of other fsl programs.
This can be suppressed by providing the '-s' flag or by setting the environment variable  DP_STANDALONE.
Depending on its configuration `fsl_sub` may itself decline to submit the job to [SGE](https://arc.liv.ac.uk/trac/SGE) or [Condor](http://en.wikipedia.org/wiki/HTCondor) and run it locally instead.
Note that although `SGE` and `fsl_sub` have a mechanism for explicitly supporting shared memory parallel environments via the '-s' option to `fsl_sub`, this is not yet supported here.

Diffprep expects nifti files to have been converted from DICOM by `dcm2nii` which includes some of the acquisition parameters required for `topup` and `eddy` in the nifti header.
If the newer tool `dcm2niix` is used for the conversion from DICOM then the information in the nifti header is insufficient.
However, in this case, using the arguments `-b y` causes `dcm2niix` to create its own json sidecar file in the standard [BIDS](http://bids.neuroimaging.io/) format.

Alternatively, a json sidecar file can be added by the local utility `dcm2js` though this is not currently in BIDS format.

If either sort of json file is present with the same base name as the images (and ending in `.json`) then `diffprep` will use it for the EPI read out time and phase encoding direction and sense rather than relying on the values in the nifti header.

R.Hartley-Davies (October 2019)

(NB Feb 2017 moved from cric/svn to bitbuckit/git)

Release History
===============

#### Version 0.5
- Pass out index.txt, acqparams.txt and data.eddy_movement_rms for use in fsl6 dti qa
- Average B0 images before passing to bet
- Tests under both fsl5 and fsl6

#### Version 0.4
- Accept BIDS style json sidecar files from dcm2niix
- Rearrange tests

#### Version 0.3
- Change threshold in bet from default 0.5 to explicit 0.3
- Move decision on whether to submit to sge until after checking command line args
- Rerun `bet` on B0 images corrected by `eddy` to get an improved brain mask
 
#### Version 0.2
- Accept low values for B value as corresponding to B zero - fixes problem with multiband diffusion
 
#### Version 0.1
- Initial refactor of original script by psjcwb
